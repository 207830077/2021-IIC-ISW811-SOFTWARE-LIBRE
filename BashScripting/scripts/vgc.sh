#!/bin/bash
# vgc (Vagrant connect) v1.0

VMSDir="$HOME/VMs"
echo "Iniciando instancia $1"
echo $VMSDir
cd $VMSDir/$1

if [[ -f "${VMSDir}/${1}/Vagrantfile" ]]
then
    cd $VMSDir/$1
    echo "Cargando agente SSH"
    eval "$(ssh-agent -s)"
    echo "Cargando llave privada"
    ssh-add .vagrant/machines/default/virtualbox/private_key
    echo "Conectando a la instancia"
    ssh -p 2222 vagrant@localhost

else
    echo "No se encontro la instancia $1 de Vagrant"
fi
