# Bash Scripting

Se inicializa la maquina vagrant

    vagrant up
    vagrant ssh

Crear una carpeta donde se almacenaran los recursos necesarios para el taller de bash scripting

    mkadir scripts

Posterior se crea un archivo **.sh** 

    touch sayhello.sh

Dentro del archivo **sayhello.sh** se agrega el siguiente código

```bash
#!/bin/bash
clear 
printf "Hola Mundo! \n"
```
Esto es un codigo simple de ***bash*** que hace es limpiar la pantalla y luego imprime un _"Hola Mundo!"_

Para ejecutar el archivo **syhello.sh**, primero debe tener perimisos de ejecución con el comando

    chmod u+x sayhello.sh

Una vez con permisos de ejecución el archivo se podra ejecutar el archivo correctamente con el siguiente comando

    ./sayhello.sh

Seguidamente se modifica el archivo **sayhello.sh** se agrega nuevas funciones

```bash
#!/bin/bash

clear 
printf "Ingrese su nombre: \n"
read name

echo 
if [ ${#name} -eq 0 ]; then
    echo "Me hubiera gustado conocerte...."
else 
    echo "Hola $name"
fi
if [ -z ${name} ]; then
    echo "Me hubiera gustado conocerte...."
else 
    echo "Hola $name"
fi

while [ -z ` echo $birthyear | grep -E ^-\?[0-9]*$` ]; do
    echo
    printf "¿En que año nacio (yyyy)? "
    read birthyear
done

printf "Año de nacimiento: $birthyear \n"
```
Se crea el archivo **vgc.sh** para conectarse a la maquina _vagrant_ con bash scripting
    
    touch vgc.sh


```bash
#!/bin/bash
# vgc (Vagrant connect) v1.0

VMSDir="$HOME/VMs"
echo "Iniciando instancia $1"
echo $VMSDir
cd $VMSDir/$1

if [[ -f "${VMSDir}/${1}/Vagrantfile" ]]
then
    cd $VMSDir/$1
    echo "Cargando agente SSH"
    eval "$(ssh-agent -s)"
    echo "Cargando llave privada"
    ssh-add .vagrant/machines/default/virtualbox/private_key
    echo "Conectando a la instancia"
    ssh -p 2222 vagrant@localhost

else
    echo "No se encontro la instancia $1 de Vagrant"
fi
```

# dotVim con esteroides

Instalando vim y git

    sudo apt-get install git curl vim vim-nox

Clonar el repositorio **mismatso/dotvim.git**

    git clone https://github.com/mismatso/dotvim.git ~/.vim

Se crea un enlace simbolico a **vimrc**

    ln -s ~/.vim/.vimrc ~/.vimrc

Ahora se instala los paquetes de complementos como submódulos de git

    cd ~/.vim
    git submodule init
    git submodule update

Luego, instale Pathogen en **~/.vim/autoload/pathogen.vim**

    mkdir -p ~/.vim/autoload ~/.vim/bundle && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

En GNU / Linux, puede instalar las fuentes parcheadas para Vim-Airline de esa manera

    ~/.vim/fonts/install.sh

# Cronjob

Se ejecuta el siguiente comando para entrar a **contrab** y especificar que desea que se ejecute

    contrab -e

Ingresar a mysql con el siguiente comando

    sudo mysql

Se crea una base de datos llamada **northwind**

     create database northwind;

Se crea un nuevo usuario y se le da todos los privilegios

    create user laravel identified by 'secret';
    grant all privileges on northwind.* to laravel@'%';
    flush privileges;

Luego de esto se procede a nesconectarse de mysql con el comando

    quit

Se restaura la base de datos **northwind** al usuario **laravel** con el siguiente comando

     mysql northwind < northwind.sql -u laravel -p
     mysql northwind < northwind-data.sql -u laravel -p

Seguidamente se creara un script que realizara backups de la base de datos. Se crea el archivo **backup.sh** con el siguiente comando

    touch backup.sh

Se agrega el siguiente codigo al archivo **backup.sh**

```bash
#!/bin/bash

echo "Iniciando respaldo..."
echo

site='northwind'
directory="/home/vagrant/backups/${site}/"
datetime=$(date +"%Y%m%d_%H%M%S")
database="northwind"
username="laravel"
password="secret"

if [ ! -z "$1" ]; then
    filename="${site}_${1}.sql"
else
    filename="${site}_${datetime}.sql"
fi

mkdir -p $directory
mysqldump $database > ${directory}${filename} -u $username --password=$password

echo "Comprimiendo el archivo de respaldo...."
cd $directory
tar cvfz ${filename}.tar.gz ${filename}

echo "Eliminando archivo temporal..."
rm ${filename}

echo "Registrando la ejecución del script"
echo ${filename} $(date +"%d/%m/%Y %H:%M:%S") >> backup_${site}.log 

echo "¡Listo!"
echo
```

Una vez realizado el script se puede ejecutar, esto en la ruta donde se tiene creado el archivo pero antes de eso se debe dar permisos de ejecución

    chmod u+x backup.sh
    ./backup.sh

Backup realizado, prueba de ello

![backup](./img/backup.png)


Luego de esto se puede realizar la ejecución del backup cada minuto, esto seria en el contrab 

    contrab -e

Dentro de **contrab** se agrega el siguiente comando

    * * * * * /home/vagrant/backups/northwind/backup.sh

Si todo sale bien deberia de realizar un backup cada minuto, la siguiente imagen es la evidencia que se esta realizando correctamente

![backup](./img/backup1min.png)    


