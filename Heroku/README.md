# Laravel on Heroku

## Paso 1

En la pagina principal de **Heroku** registrase y acceder a la cuenta, esto es necesario para publicar el proyecto en la plataforma heroku

    Link de acceso: https://www.heroku.com/

## Paso 2

Instalar dependecias necesarias para tener el CLI de heroku

__Instalar GNUPG y CURL__

    sudo apt-get install gnupg curl

__Instalar CLI de Heroku__

    curl https://cli-assets.heroku.com/install-ubuntu.sh | sh

## Paso 3

Crear un proyecto Laravel via composer

    composer create-project laravel/laravel blog

## Paso 4

Versionar el proyecto 

    git init
    git add .
    git commit -m "Blog heroku"

## Paso 5

Loguearse con heroku via CLI

    heroku login

![1](img/1.png)

![2](img/2.png)


## Paso 6

Crear un archivo Procfile

    echo "web: vendor/bin/heroku-php-apache2 public/" > Procfile

Agregar cambios al versionamiento

    git init
    git add .
    git commit -m "Procfile for Heroku"

## Paso 7

Crear una nueva aplicación en Heroku

    heroku create

Luego de esto se creara y posteara la aplicación en heroku

![3](img/3.png)

![4](img/4.png)

## Paso 8

Agregar APP_KEY en heroku

    cat .env | grep APP_KEY

El comando anterior muestrar la clave, luego de esto se procede a setear en heroku

    heroku config:set APP_KEY=base64:b7OtZIgl8i6Sq5JNSUVJGUOx2q+RCWSFuLbqF/s6y0M=

![5](img/5.png)

## Paso 9

Instalar dependecias laravel/ui

    composer require laravel/ui
    php artisan ui bootstrap
    php artisan ui bootstrap --auth
    npm install && npm run dev

    git init
    git add .
    git commit "Laravel/ui"

## Paso 10

Subir el proyecto a Heroku

    git push heroku master

## Paso 11

Heroku Postgresql

    heroku addons:create heroku-postgresql:hobby-dev

![6](img/6.png)

## Paso 12

Modificar el archivo de configuración de base de datos

En el archivo **config/database.php** justo debajo de **<?php** colocamos las siguientes líneas de código

```php
$url = parse_url(getenv("DATABASE_URL"));
```

Luego en el mismo archivo , en el apartado de connections se agrega el siguiente arreglo

```php
'pgsql-hobby-dev' => [
 'driver' => 'pgsql',
 'url' => env('DATABASE_URL'),
 'host' => env('DB_HOST', $url["host"]),
 'port' => env('DB_PORT', $url["port"]),
 'database' => env('DB_DATABASE', substr($url["path"],1)),
 'username' => env('DB_USERNAME', $url["user"]),
 'password' => env('DB_PASSWORD', $url["pass"]),
 'charset' => 'utf8',
 'prefix' => '',
 'prefix_indexes' => true,
 'schema' => 'public',
 'sslmode' => 'prefer',
 ],
 ```

## Paso 13

Subir cambios 

    git init
    git add .
    git commit "Agregación Database"

    git push heroku master

## Paso 14

Listo con esto quedaria funcionando la aplicación en heroku con una base de datos postfresql

Puede acceder al suguiente enlace y darse de alta en la aplicación

    https://hello-heroku-isw811.herokuapp.com/

