# Base de Datos

## Paso 1

Conectarse a la maquina virtual 

    vagrant up
    vagrant ssh

## Paso 2

Crear la base de datos con el nombre **northwind**
    
    CREATE DATABASE northwind;

## Paso 3

Crear un usuario con el nombre **laravel**

    CREATE USER 'laravel'@'%' IDENTIFIED BY 'secret';

## Paso 4

Dar los permisos al usuario **laravel** a la base de datos **northwind**

    GRANT ALL PRIVILEGES ON northwind.* TO 'laravel'@'%';
    FLUSH PRIVILEGES;

## Paso 5

Restaurar la base de datos **northwind**

    $ mysql northwind < northwind.sql -u root -p 
    $ mysql northwind < northwind-data.sql -u root -p

## Paso 6

Conectarse con MYSQL Workbech

![msql_connection](./img/mysql.PNG)
# Ejercicios

## Challenge #1

Recupere el código (id) y la descripción (type_name) de los tipos de movimiento de inventario (inventory_transaction_types).

     SELECT id código, type_name descripción 
        FROM inventory_transaction_types;

![1](./img/1.PNG)    

## Challenge #2

Recupere la cantidad de ordenes (orders) registradas por cada vendedor (employees).

    SELECT concat(e.first_name, ' ', e.last_name) `Vendedor`, count(1) `Cantidad` 
        FROM orders o 
        JOIN employees e 
            ON e.id = o.employee_id 
        GROUP BY e.id;

![2](./img/2.PNG)

## Challenge #3

Recupere la lista de los 10 productos más ordenados (order_details), y la cantidad total de unidades ordenadas para cada uno de los productos. Deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la cantidad de unidades.

    SELECT 	p.product_code `Código`, 
            p.product_name `Producto`,
            round(sum(od.quantity), 2) `Cantidad` 
        FROM order_details od
        JOIN products p 
            ON p.id = od.product_id 
        GROUP BY p.id
        ORDER BY Cantidad DESC;

![3](./img/3.PNG)

## Challenge #4

Recupere el monto total (invoices, orders, order_details, products) y la cantidad de facturas (invoices) por vendedor (employee). Debe considerar solamente las ordenes con estado diferente de 0 y solamente los detalles en estado 2 y 3, debe utilizar el precio unitario de las lineas de detalle de orden, no considere el descuento, no considere los impuestos porque la comisión a los vendedores se paga sobre el precio base.

    SELECT 	count(1) `Cantidad`,
            concat(e.first_name, ' ', e.last_name) `Vendedor`,
            round(sum(od.quantity * od.unit_price), 2) `Monto Facturado`
        FROM invoices i
        JOIN orders o
            ON o.id = i.order_id
        JOIN order_details od
            ON od.order_id = o.id
        JOIN employees e
            ON e.id = o.employee_id
        WHERE o.status_id <> 0
            AND od.status_id IN (2, 3)
        GROUP BY e.id
        ORDER BY Cantidad DESC, Vendedor ASC;

![4](./img/4.PNG)

## Challenge #5
Recupere los movimientos de inventario del tipo ingreso. Tomando como base todos los movimientos de inventario (inventory_transactions), considere unicamente el tipo de movimiento 1 (transaction_type) como ingreso. Debe agrupar por producto (inventory_transactions.product_id) y deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la cantidad de unidades ingresadas.

    SELECT 	p.product_code `Código`, 
            p.product_name `Producto`,
            it.quantity `Cantidad`
        FROM products p
        JOIN inventory_transactions it
            ON p.id = it.product_id
        WHERE it.transaction_type = 1
        GROUP BY it.product_id;

![5](./img/5.PNG)

## Challenge #6

Recupere los movimientos de inventario del tipo salida. Tomando como base todos los movimientos de inventario (inventory_transactions), considere unicamente los tipos de movimiento (transaction_type) 2, 3 y 4 como salidas. Debe agrupar por producto (products) y deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la cantidad de unidades que salieron.

    SELECT  p.product_code `Código`,
            p.product_name `Producto`,
            sum(it.quantity) `Cantidad`
        FROM products p
        JOIN  inventory_transactions it
            ON p.id = it.product_id
        WHERE it.transaction_type IN (2, 3, 4)
        GROUP BY p.id;

![6](./img/6.PNG)

## Challenge #7

Genere un reporte de movimientos de inventario (inventory_transactions) por producto (products), tipo de transacción y fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas fechas). Debe incluir como mínimo el código (product_code), el nombre del producto (product_name), la fecha truncada (transaction_created_date), la descripción del tipo de movimiento (type name) y la suma de cantidad (quantity) .


    SELECT 	p.product_code `Código`,
            p.product_name `Producto`,
            it.type_name `Tipo`,
            DATE_FORMAT(i.transaction_created_date, '%d/%m/%Y') `Fecha`,
            sum(i.quantity) `Cantidad`
        FROM products p
        JOIN inventory_transactions i
            ON p.id = i.product_id
        JOIN inventory_transaction_types it
            ON i.transaction_type = it.id
        WHERE i.transaction_created_date 
            BETWEEN	'2006-03-22' AND '2006-03-24'
        GROUP BY p.product_name, i.transaction_type;

![7](./img/7.PNG)

## Challenge #8
Genere la consulta SQL para un reporte de inventario, tomando como base todos los movimientos de inventario (inventory_transactions), considere los tipos de movimientO (transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso. Este reporte debe estar agrupado por producto (products) y deberá incluir como mínimo los campos de código (product_code), nombre del producto (product_name) y la sumarización de ingresos, salidas y la cantidad disponible en inventario (diferencia de ingresos - salidas).


    SELECT 	p.product_code `Código`,
            p.product_name `Producto`,
            sum(if(i.transaction_type = 1, i.quantity, 0)) `Ingreso`,
            sum(if(i.transaction_type IN (2, 3, 4), i.quantity, 0)) `Salida`,
            (sum(if(i.transaction_type = 1, i.quantity, 0)) - sum(if(i.transaction_type IN (2, 3, 4), i.quantity, 0))) `Disponible`
        FROM products p
        JOIN inventory_transactions i
            ON p.id = i.product_id
        JOIN inventory_transaction_types it
            ON it.id = i.transaction_type
        GROUP BY p.id

![8](./img/8.PNG)





