/*Challenge #7
Genere un reporte de movimientos de inventario
(inventory_transactions) por producto (products), tipo de transacción y
fecha, entre las fechas 22/03/2006 y 24/03/2006 (incluyendo ambas
fechas). Debe incluir como mínimo el código (product_code), el nombre del
producto (product_name), la fecha truncada
(transaction_created_date), la descripción del tipo de movimiento
(type name) y la suma de cantidad (quantity) .*/

DESC inventory_transactions;
DESC inventory_transaction_types;

SELECT 	p.product_code `Código`,
		p.product_name `Producto`,
        it.type_name `Tipo`,
        DATE_FORMAT(i.transaction_created_date, '%d/%m/%Y') `Fecha`,
        sum(i.quantity) `Cantidad`
	FROM products p
    JOIN inventory_transactions i
		ON p.id = i.product_id
	JOIN inventory_transaction_types it
		ON i.transaction_type = it.id
	WHERE i.transaction_created_date 
		BETWEEN	'2006-03-22' AND '2006-03-24'
	GROUP BY p.product_name, i.transaction_type;