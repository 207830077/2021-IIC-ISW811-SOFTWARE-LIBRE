/*Challenge #8
Genere la consulta SQL para un reporte de inventario, tomando como base todos los
movimientos de inventario (inventory_transactions), considere los tipos de movimiento
(transaction_type) 2, 3 y 4 como salidas y el tipo 1 como ingreso. Este reporte debe estar agrupado por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto (product_name) y
la sumarización de ingresos, salidas y la cantidad disponible en inventario (diferencia
de ingresos - salidas).*/

DESC inventory_transactions;
SELECT 	p.product_code `Código`,
		p.product_name `Producto`,
        sum(if(i.transaction_type = 1, i.quantity, 0)) `Ingreso`,
        sum(if(i.transaction_type IN (2, 3, 4), i.quantity, 0)) `Salida`,
        (sum(if(i.transaction_type = 1, i.quantity, 0)) - sum(if(i.transaction_type IN (2, 3, 4), i.quantity, 0))) `Disponible`
	FROM products p
	JOIN inventory_transactions i
		ON p.id = i.product_id
	JOIN inventory_transaction_types it
		ON it.id = i.transaction_type
	GROUP BY p.id