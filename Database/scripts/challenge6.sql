/*Challenge #6
Recupere los movimientos de inventario del tipo salida. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente los tipos de
movimiento (transaction_type) 2, 3 y 4 como salidas.
Debe agrupar por producto (products) y deberá incluir como
mínimo los campos de código (product_code), nombre del producto
(product_name) y la cantidad de unidades que salieron.*/

DESC inventory_transactions;

SELECT  p.product_code `Código`,
		p.product_name `Producto`,
        sum(it.quantity) `Cantidad`
    FROM products p
    JOIN  inventory_transactions it
		ON p.id = it.product_id
	WHERE it.transaction_type IN (2, 3, 4)
    GROUP BY p.id;