/*Challenge #5
Recupere los movimientos de inventario del tipo ingreso. Tomando
como base todos los movimientos de inventario
(inventory_transactions), considere unicamente el tipo de movimiento
1 (transaction_type) como ingreso.
Debe agrupar por producto (inventory_transactions.product_id) y
deberá incluir como mínimo los campos de código (product_code),
nombre del producto (product_name) y la cantidad de unidades
ingresadas.*/

DESC inventory_transactions;

SELECT 	p.product_code `Código`, 
		p.product_name `Producto`,
        it.quantity `Cantidad`
	FROM products p
    JOIN inventory_transactions it
		ON p.id = it.product_id
	WHERE it.transaction_type = 1
    GROUP BY it.product_id;