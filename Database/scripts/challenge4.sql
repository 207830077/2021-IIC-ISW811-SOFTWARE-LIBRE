/*Challenge #4
Recupere el monto total (invoices, orders, order_details, products) y la
cantidad de facturas (invoices) por vendedor (employee). Debe
considerar solamente las ordenes con estado diferente de 0 y
solamente los detalles en estado 2 y 3, debe utilizar el precio
unitario de las lineas de detalle de orden, no considere el descuento,
no considere los impuestos, porque la comisión a los vendedores se
paga sobre el precio base.*/

DESC invoices;
DESC orders;
DESC order_details;
DESC products;
DESC employees;

SELECT 	count(1) `Cantidad`,
		concat(e.first_name, ' ', e.last_name) `Vendedor`,
        round(sum(od.quantity * od.unit_price), 2) `Monto Facturado`
	FROM invoices i
    JOIN orders o
		ON o.id = i.order_id
	JOIN order_details od
		ON od.order_id = o.id
	JOIN employees e
		ON e.id = o.employee_id
	WHERE o.status_id <> 0
		AND od.status_id IN (2, 3)
	GROUP BY e.id
    ORDER BY Cantidad DESC, Vendedor ASC;