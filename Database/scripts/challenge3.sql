/*Challenge #3
Recupere la lista de los 10 productos más ordenados (order_details),
y la cantidad total de unidades ordenadas para cada uno de los
productos.*/

DESC order_details;
DESC products;

	SELECT 	p.product_code `Código`, 
			p.product_name `Producto`,
			round(sum(od.quantity), 2) `Cantidad` 
		FROM order_details od
		JOIN products p 
			ON p.id = od.product_id 
		GROUP BY p.id
		ORDER BY Cantidad DESC;
