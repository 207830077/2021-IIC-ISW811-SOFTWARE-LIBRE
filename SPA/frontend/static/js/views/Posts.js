import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Posts");
    }

    async getHtml() {
        return `
            <h1>Posts</h1>
            <p>You are viewing the posts!</p>
            <p>
                <a href="/posts/1" data-link>First Post</a>
            </p>
            <p>
                <a href="/posts/2" data-link>Second Post</a>
            </p>
            <p>
                <a href="/posts/3" data-link>Thied Post</a>
            </p>
            
        `;
    }
}