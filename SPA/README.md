# Single Page Application (SPA) with JavaScript

## Paso 1

Inicializar el proyecto 

    npm init -y

## Paso 2

Incluir express

    npm i express

## Paso 3

Crear un archivo javascript con el siguiente nombre:

    server.js

## Paso 4

Crear una carpeta con el nombre **frontend** dentro de esta carpeta crear un archivo **index.html**

## Paso 5

En el archivo **frontend/index.html** digitar el siguiente codigo 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Single Page App</title>
    <link rel="stylesheet" href="/static/css/index.css">
</head>
<body>
    <nav class="nav">
        <a href="/" class="nav__link" data-link>Dashboard</a>
        <a href="/posts" class="nav__link" data-link>Posts</a>
        <a href="/settings" class="nav__link" data-link>Settings</a>
    </nav>
    <div id="app"></div>
    <script type="module" src="/static/js/index.js"></script>
</body>
</html>
```

## Paso 6

En el archivo **server.js** digitar el siguiente codigo

```js
const express = require("express");
const path = require("path");

const app = express();

app.use("/static", express.static(path.resolve(__dirname, "frontend", "static")));

app.get("/*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "frontend", "index.html"));
});

app.listen(process.env.PORT || 8080, () => console.log("Server running..."));
```

## Paso 7

Dentro de la carpeta **frontend** crear una nueva carpeta con el nombre **static**, además dentro de dicha carpeta incluir una nueva con el nombre **js**. Dentro de la carpeta **js** crear un archivo con siguiente nombre

    index.js

## Paso 8

En el archivo **frontend/static/js/index.js** digitar el siguiente codigo

```js
import Dashboard from "./views/Dashboard.js";
import Posts from "./views/Posts.js";
import PostView from "./views/PostView.js";
import Settings from "./views/Settings.js";

const pathToRegex = path => new RegExp("^" + path.replace(/\//g, "\\/").replace(/:\w+/g, "(.+)") + "$");

const getParams = match => {
    const values = match.result.slice(1);
    const keys = Array.from(match.route.path.matchAll(/:(\w+)/g)).map(result => result[1]);

    return Object.fromEntries(keys.map((key, i) => {
        return [key, values[i]];
    }));
};

const navigateTo = url => {
    history.pushState(null, null, url);
    router();
};

const router = async () => {
    const routes = [
        { path: "/", view: Dashboard },
        { path: "/posts", view: Posts },
        { path: "/posts/:id", view: PostView },
        { path: "/settings", view: Settings }
    ];

    // Test each route for potential match
    const potentialMatches = routes.map(route => {
        return {
            route: route,
            result: location.pathname.match(pathToRegex(route.path))
        };
    });

    let match = potentialMatches.find(potentialMatch => potentialMatch.result !== null);

    if (!match) {
        match = {
            route: routes[0],
            result: [location.pathname]
        };
    }

    const view = new match.route.view(getParams(match));

    document.querySelector("#app").innerHTML = await view.getHtml();
};

window.addEventListener("popstate", router);

document.addEventListener("DOMContentLoaded", () => {
    document.body.addEventListener("click", e => {
        if (e.target.matches("[data-link]")) {
            e.preventDefault();
            navigateTo(e.target.href);
        }
    });

    router();
});
```

## Paso 9

Dentro de la carpeta **static/js** crear una nueva carpeta con el nombre **views**. Luego de esto crear varios archivos **.js** con su respectivo codigo, a continuación se mostrara el contenido que debe llevar cada archivo.


* **AbstractView.js**

```js
export default class {
    constructor(params) {
        this.params = params;
    }

    setTitle(title) {
        document.title = title;
    }

    async getHtml() {
        return "";
    }
}
```

* **Dashboard.js**

```js
import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Dashboard");
    }

    async getHtml() {
        return `
            <h1>Welcome back, Dom</h1>
            <p>
                Fugiat voluptate et nisi Lorem cillum anim sit do eiusmod occaecat irure do. Reprehenderit anim fugiat sint exercitation consequat. Sit anim laborum sit amet Lorem adipisicing ullamco duis. Anim in do magna ea pariatur et.
            </p>
            <p>
                <a href="/posts" data-link>View recent posts</a>.
            </p>
        `;
    }
}
```

* **Posts.js**

```js
import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Posts");
    }

    async getHtml() {
        return `
            <h1>Posts</h1>
            <p>You are viewing the posts!</p>
            <p>
                <a href="/posts/1" data-link>First Post</a>
            </p>
            <p>
                <a href="/posts/2" data-link>Second Post</a>
            </p>
            <p>
                <a href="/posts/3" data-link>Thied Post</a>
            </p>
            
        `;
    }
}
```

* **PostView.js**

```js
import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.postId = params.id;
        this.setTitle("Viewing Post");
    }

    async getHtml() {
        return `
            <h1>Post</h1>
            <p>You are viewing post #${this.postId}.</p>
            <a href="/posts" data-link>Back</a>
        `;
    }
}
```

* **Settings.js**

```js
import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor(params) {
        super(params);
        this.setTitle("Settings");
    }

    async getHtml() {
        return `
            <h1>Settings</h1>
            <p>Manage your privacy and configuration.</p>
        `;
    }
}
```

## Paso 10

Se debe crear una carpeta **css** dentro de la carpeta **static**. Dentro de la carpeta **css** se crea un archivo de estilo con el nombre **index.css**, se debe agregar el siguiente codigo de estilo para la pagina.

* **index.css**

```css
body {
    --nav-width: 200px;
    margin: 0 0 0 var(--nav-width);
    font-family: 'Quicksand', sans-serif;
    font-size: 18px;
}

.nav {
    position: fixed;
    top: 0;
    left: 0;
    width: var(--nav-width);
    height: 100vh;
    background: #222222;
}

.nav__link {
    display: block;
    padding: 12px 18px;
    text-decoration: none;
    color: #eeeeee;
    font-weight: 500;
}

.nav__link:hover {
    background: rgba(255, 255, 255, 0.05);
}

#app {
    margin: 2em;
    line-height: 1.5;
    font-weight: 500;
}

a {
    color: #009579;
}
```

## Paso 11

Para ejecutar el proyecto se debe hacer con el siguiente comando

    node server.js


Con esto terminaria el manual sobre Single Page Application (SPA) utilizando JavaScript
